
	$(document).ready(function(){
		$('.gallery1').galleryScroll({    
			btPrev: 'a.link-prev',  
			btNext: 'a.link-next',   
			holderList: 'div', 
			scrollElParent: 'ul',    
			scrollEl: 'li',         
			slideNum: false,        
			duration : 300,  
			step: 1,             
			circleSlide: false,       
			disableClass: 'disable', 
			funcOnclick: null        
		});
		$('#main-event').galleryScroll({
			autoSlide: 4000,
			circleSlide: true,
			duration: 600
		});
		$('div.g2').galleryScroll({
			step:1,
			duration:300,
			funcOnclick:function(){
			$('p.margin').html($('ul',this).css('marginLeft'));
			}
		});
		$('div.g3').galleryScroll({
			step:1,
			duration: 800
			
		});
$('.menu-map a').click(function(){
	$('.menu-map li').removeClass('active')
	$(this).parent().addClass('active')
	$('.district').hide()
	var id = $(this).attr('rel');
	$('#'+id).fadeIn()
	return false;
})

$('.anthem-button .play').click(function(){
	$('.anthem-button .play').hide();
	$('.anthem-button .pause').show();
	setTimeout("$('.anthem .string').show();", 28000);
	}
);
$('.anthem-button .pause').click(function(){
	$('.anthem-button .play').show();
	$('.anthem-button .pause').hide();
	$('.anthem .string').hide();
	}
);

$('.num-2,.num-3,.num-4,.num-5,.num-6,.num-7,.num-8,.num-9,.num-10').css('display','none');
$('.pagi-1').addClass('selected');


$('.pagi-1').click(function(){
	$('.num-1').show();
	$('.num-2,.num-3,.num-4,.num-5,.num-6,.num-7,.num-8,.num-9,.num-10').hide();
	$('.pagi-1').addClass('selected');
	$('.pagi-2,.pagi-3,.pagi-4,.pagi-5,.pagi-6,.pagi-7,.pagi-8,.pagi-9,.pagi-10').removeClass('selected');
	}
);
$('.pagi-2').click(function(){
	$('.num-2').show();
	$('.num-1,.num-3,.num-4,.num-5,.num-6,.num-7,.num-8,.num-9,.num-10').hide();
	$('.pagi-2').addClass('selected');
	$('.pagi-1,.pagi-3,.pagi-4,.pagi-5,.pagi-6,.pagi-7,.pagi-8,.pagi-9,.pagi-10').removeClass('selected');
	}
);
$('.pagi-3').click(function(){
	$('.num-3').show();
	$('.num-1,.num-2,.num-4,.num-5,.num-6,.num-7,.num-8,.num-9,.num-10').hide();
	$('.pagi-3').addClass('selected');
	$('.pagi-2,.pagi-1,.pagi-4,.pagi-5,.pagi-6,.pagi-7,.pagi-8,.pagi-9,.pagi-10').removeClass('selected');
	}
);
$('.pagi-4').click(function(){
	$('.num-4').show();
	$('.num-1,.num-3,.num-2,.num-5,.num-6,.num-7,.num-8,.num-9,.num-10').hide();
	$('.pagi-4').addClass('selected');
	$('.pagi-2,.pagi-3,.pagi-1,.pagi-5,.pagi-6,.pagi-7,.pagi-8,.pagi-9,.pagi-10').removeClass('selected');
}
);
$('.pagi-5').click(function(){
	$('.num-5').show();
	$('.num-1,.num-2,.num-4,.num-3,.num-6,.num-7,.num-8,.num-9,.num-10').hide();
	$('.pagi-5').addClass('selected');
	$('.pagi-2,.pagi-3,.pagi-4,.pagi-1,.pagi-6,.pagi-7,.pagi-8,.pagi-9,.pagi-10').removeClass('selected');
	}
);
$('.pagi-6').click(function(){
	$('.num-6').show();
	$('.num-1,.num-2,.num-4,.num-5,.num-3,.num-7,.num-8,.num-9,.num-10').hide();
	$('.pagi-6').addClass('selected');
	$('.pagi-2,.pagi-3,.pagi-4,.pagi-5,.pagi-1,.pagi-7,.pagi-8,.pagi-9,.pagi-10').removeClass('selected');
	}

);
$('.pagi-7').click(function(){
	$('.num-7').show();
	$('.num-1,.num-2,.num-4,.num-5,.num-6,.num-3,.num-8,.num-9,.num-10').hide();
	$('.pagi-7').addClass('selected');
	$('.pagi-2,.pagi-3,.pagi-4,.pagi-5,.pagi-6,.pagi-1,.pagi-8,.pagi-9,.pagi-10').removeClass('selected');
	}
);
$('.pagi-8').click(function(){
	$('.num-8').show();
	$('.num-1,.num-2,.num-4,.num-5,.num-6,.num-7,.num-3,.num-9,.num-10').hide();
	$('.pagi-8').addClass('selected');
	$('.pagi-2,.pagi-3,.pagi-4,.pagi-5,.pagi-6,.pagi-7,.pagi-1,.pagi-9,.pagi-10').removeClass('selected');
	}
);
$('.pagi-9').click(function(){
	$('.num-9').show();
	$('.num-1,.num-2,.num-4,.num-5,.num-6,.num-7,.num-8,.num-3,.num-10').hide();
	$('.pagi-9').addClass('selected');
	$('.pagi-2,.pagi-3,.pagi-4,.pagi-5,.pagi-6,.pagi-7,.pagi-8,.pagi-1,.pagi-10').removeClass('selected');
	}
);
$('.pagi-10').click(function(){
	$('.num-10').show();
	$('.num-1,.num-2,.num-4,.num-5,.num-6,.num-7,.num-8,.num-9,.num-3').hide();
	$('.pagi-10').addClass('selected');
	$('.pagi-2,.pagi-3,.pagi-4,.pagi-5,.pagi-6,.pagi-7,.pagi-8,.pagi-9,.pagi-1').removeClass('selected');
	}
);

$('.numb-1, .numb-2,.numb-3, .numb-4,.numb-5,.numb-6,.numb-7').css('display','none');

$('.width-1').click(function(){
	$('.numb-1').show();
	$('.numb-2').hide();
	$('.numb-3').hide();
	$('.numb-4').hide();
	}
);
$('.width-2').click(function(){
	$('.numb-1').hide();
	$('.numb-2').show();
	$('.numb-3').hide();
	$('.numb-4').hide();
	}
);

$('.width-3').click(function(){
	$('.numb-1').hide();
	$('.numb-2').hide();
	$('.numb-3').show();
	$('.numb-4').hide();
	}
);

$('.width-4').click(function(){
	$('.numb-1').hide();
	$('.numb-2').hide();
	$('.numb-3').hide();
	$('.numb-4').show();
	}
);
$('.pag').click(function(){
	$('.numb-1').hide();
	$('.numb-2').hide();
	$('.numb-3').hide();
	$('.numb-4').hide();
	}
);

});

/**
 * Фронт-енд пользователи
 */
jQuery(document).ready(function($){

    /**
     * Клик по ссылке "войти" - открытие/закрытие формы
     */
    $('#open-login-form').click(function(e) {
        e.preventDefault();
        e.stopPropagation();

        if ($('#login-form-popup').is(':visible')) {
            $('#login-form-popup').hide();
        } else {
            $('#login-form-popup').show();
        }
    });

    /**
     * Клик за формой авторизации - закрытие формы
     */
    $('html').click(function(e) {
        if ($(e.target).parents().filter('#login-form-popup:visible').length != 1) {
            $('#login-form-popup').hide();
        }
    });
});

